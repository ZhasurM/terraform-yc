# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/telmate/proxmox" {
  version     = "2.9.3"
  constraints = "2.9.3"
  hashes = [
    "h1:ncggek/h3O6fvTrni58gn4gwMsTuaXY6OZ5doRrHn/o=",
  ]
}
