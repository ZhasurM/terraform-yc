#!/bin/bash


#Colors settings
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

apt update && apt upgrade -y
clear

# # Проверяем, запущен ли скрипт от имени root пользователя
# if [[ $EUID -ne 0 ]]; then
#    echo -e "${RED}Этот скрипт должен быть запущен от имени root!${NC}" 
#    return 1
# fi

# # 
# if [[ $# != 4 ]]; then
#     echo -e "${YELLOW}Переданы не все параметры${NC}. 
# Пример ->
# ${BLUE}test1.sh <TimeZome> <ssh_port> <user_name> <password>
# test1.sh Asia/Almaty 2224 usertest userpass${NC}"
#     return 1
# fi

# echo "Starting set up "
# # Настройка часового пояса
# timedatectl set-timezone $1

# # Настройка локали
# update-locale LANG=LANG=en_IN.UTF-8 LANGUAGE

# # Изменение порта ssh
# sed -i 's/Port \+[0-9]*\|#Port \+[0-9]*/Port $2/g' /etc/ssh/sshd_config

# # Deny remote login as root user
# sed -i '/^PermitRootLogin \+yes/s/^/#/' /etc/ssh/sshd_config

# # Create user
# useradd -m -s /bin/bash $3
# echo "$3:$4" | chpasswd




# echo "Установка Nginx"

# NGINX=$(dpkg-query -W -f='${Status}' nginx 2>/dev/null | grep -c "ok installed")
#   if [ $(dpkg-query -W -f='${Status}' nginx 2>/dev/null | grep -c "ok installed") -eq 0 ];
#   then
#     echo -e "${YELLOW}Installing nginx${NC}"
#     apt-get install nginx php5-fpm --yes;
#     elif [ $(dpkg-query -W -f='${Status}' nginx 2>/dev/null | grep -c "ok installed") -eq 1 ];
#     then
#       echo -e "${GREEN}nginx is installed!${NC}"
#   fi

function install_pack() {
  if [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -eq 0 ];
  then
    echo -e "${YELLOW}Installing $1${NC}"
    apt-get install $1 --yes;
  elif [ $(dpkg-query -W -f='${Status}' $1 2>/dev/null | grep -c "ok installed") -eq 1 ];
  then
      echo -e "${GREEN}$1 is installed!${NC}"
  fi
}


packages=( zlib1g-dev libpam0g-dev libssl-devlibtool bison flex autoconf gcc make nginx )

for i in "${packages[@]}"
do 
  install_pack $i
done


# Install Monit

git clone https://tildeslash@bitbucket.org/tildeslash/monit.git