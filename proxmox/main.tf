resource "proxmox_vm_qemu" "srv_test" {
  count       = length(var.dict_test)
  name        = "${var.dict_test[count.index].name}"
  desc        = "Ubuntu server"
  target_node = "pve"

  agent = 0

  clone      = "ubuntu-server-focal"
  full_clone = true
  cores      = 2
  sockets    = 1
  cpu        = "host"
  memory     = 2048

  network {
    bridge = "vmbr0"
    model  = "virtio"
  }

  disk {
    storage = "local-lvm"
    type    = "virtio"
    size    = "20G"
  }

  os_type    = "cloud-init"
  ipconfig0  = "${var.dict_test[count.index].ifconfig}"
  nameserver = "8.8.8.8"
  ciuser     = "ubuntu"
  sshkeys    = file("~/.ssh/id_rsa.pub")

}
