# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.2.3"
  hashes = [
    "h1:aWp5iSUxBGgPv1UnV5yag9Pb0N+U1I0sZb38AXBFO8A=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.84.0"
  hashes = [
    "h1:77x7WR5CNtpWlo4h+Y3FIbhGMzE6IQpJN+foZktGDcQ=",
    "h1:8R3T4MZsUK+0ivHPbfgKB/Ho4SNRKbho0sMv5Tf4K5w=",
  ]
}
