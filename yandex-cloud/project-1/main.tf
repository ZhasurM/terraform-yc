provider "yandex" {
  zone = "ru-central1-a"
}

locals {
  list_vms_managers = [
    {
      name          = "manager-1"
      core_fraction = 5
      cores         = 2
      memory        = 2
      disk_size     = 20
      platform_id   = "standard-v1"
      image_id      = "fd8kb72eo1r5fs97a1ki"
      ssh-keys      = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      subnet_id     = module.vpc-dev1.my_subnet_id[0]
    },
    {
      name          = "manager-2"
      core_fraction = 5
      cores         = 2
      memory        = 2
      disk_size     = 20
      platform_id   = "standard-v1"
      image_id      = "fd8kb72eo1r5fs97a1ki"
      ssh-keys      = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      subnet_id     = module.vpc-dev1.my_subnet_id[0]
    }
  ]
  list_vms_workers = [
    {
      name          = "worker-1"
      core_fraction = 5
      cores         = 2
      memory        = 2
      disk_size     = 20
      platform_id   = "standard-v1"
      image_id      = "fd8kb72eo1r5fs97a1ki"
      ssh-keys      = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      subnet_id     = module.vpc-dev1.my_subnet_id[0]
    },
    {
      name          = "worker-2"
      core_fraction = 5
      cores         = 2
      memory        = 2
      disk_size     = 20
      platform_id   = "standard-v1"
      image_id      = "fd8kb72eo1r5fs97a1ki"
      ssh-keys      = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      subnet_id     = module.vpc-dev1.my_subnet_id[0]
    }
  ]
}

module "vpc-dev1" {
  source       = "../modules/network"
  network_name = "minio_lab"
  env          = "dev"
  subnets      = ["10.10.0.0/24"]
  zone         = "ru-central1-a"
}

locals {
  local_subnet_id_1 = module.vpc-dev1.my_subnet_id[0]
  local_subnet_id_2 = module.vpc-dev1.my_subnet_id[0]
}


module "compute_instance" {
  source            = "../modules/compute_instance"
  auto_delete       = true
  zone              = "ru-central1-a"
  list_vms_managers = local.list_vms_managers
  list_vms_workers  = local.list_vms_workers
  depends_on = [
    module.vpc-dev1
  ]

}

resource "local_file" "create_template" {
  content = templatefile("inventory.yaml.tftpl",
    {
      managers = module.compute_instance.instances_details_managers,
      workers  = module.compute_instance.instances_details_workers
    }
  )
  filename = "./inventory.yaml"
}

output "my_network_id" {
  value = module.vpc-dev1.my_network_id
}
output "my_subnet_id" {
  value = module.vpc-dev1.my_subnet_id
}

output "instances_details_managers" {
  value = module.compute_instance
}
