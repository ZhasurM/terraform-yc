# Создаем сеть
resource "yandex_vpc_network" "network-a" {
  name = var.network_name
}

# Создаем подсеть 
resource "yandex_vpc_subnet" "subnet-a" {
  count = length(var.subnets)
  v4_cidr_blocks = [element(var.subnets, count.index)]
  #zone           = var.zone
  network_id     = yandex_vpc_network.network-a.id
  # route_table_id = yandex_vpc_route_table.nat1.id
}

 # Создаем шлюз
# resource "yandex_vpc_gateway" "main_gw" {
#   name = "gateway"
#   shared_egress_gateway {}
# }

# resource "yandex_vpc_route_table" "nat1" {
#   network_id = yandex_vpc_network.network-a.id

#   static_route {
#     destination_prefix = "0.0.0.0/0"
#     gateway_id         = yandex_vpc_gateway.main_gw.id
#   }
# }