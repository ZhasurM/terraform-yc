variable "network_name" {
  type        = string
  default     = "default_network_name"
  description = "YC network"
}

variable "env" {
  default = "dev"
}

variable "subnets" {
  default     = ["10.10.1.0/24", "10.10.2.0/24"]
  description = "Subnet for VMs"
}

variable "zone" {
  type    = string
  default = "ru-central1-a"
}

