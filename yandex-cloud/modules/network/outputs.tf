output "my_network_id" {
  value = yandex_vpc_network.network-a.id
}

output "my_subnet_id" {
  value = [
    for i in yandex_vpc_subnet.subnet-a:
    "${i.id}"
  ]
}