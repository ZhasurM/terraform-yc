resource "yandex_compute_instance" "vms-managers" {
  count       = length(var.list_vms_managers)
  name        = var.list_vms_managers[count.index].name
  hostname    = var.list_vms_managers[count.index].name
  platform_id = var.list_vms_managers[count.index].platform_id
  zone        = var.zone


  resources {
    core_fraction = var.list_vms_managers[count.index].core_fraction # Доля vCPU
    cores         = var.list_vms_managers[count.index].cores         # vCPU
    memory        = var.list_vms_managers[count.index].memory        # RAM
  }

  boot_disk {
    auto_delete = var.auto_delete #(Optional) Defines whether the disk will be auto-deleted when the instance is deleted. The default value is True.
    initialize_params {
      image_id = var.list_vms_managers[count.index].image_id # Ubuntu 20.04
      size     = var.list_vms_managers[count.index].disk_size
    }
  }


  metadata = {
    ssh-keys = var.list_vms_managers[count.index].ssh-keys
    #user-data = "${file("user-meta.txt")}"
  }

  network_interface {
    subnet_id = var.list_vms_managers[count.index].subnet_id
    nat       = true
  }

}


resource "yandex_compute_instance" "vms-workers" {
  count       = length(var.list_vms_workers)
  name        = var.list_vms_workers[count.index].name
  hostname    = var.list_vms_workers[count.index].name
  platform_id = var.list_vms_workers[count.index].platform_id
  zone        = var.zone


  resources {
    core_fraction = var.list_vms_workers[count.index].core_fraction # Доля vCPU
    cores         = var.list_vms_workers[count.index].cores         # vCPU
    memory        = var.list_vms_workers[count.index].memory        # RAM
  }

  boot_disk {
    auto_delete = var.auto_delete #(Optional) Defines whether the disk will be auto-deleted when the instance is deleted. The default value is True.
    initialize_params {
      image_id = var.list_vms_workers[count.index].image_id # Ubuntu 20.04
      size     = var.list_vms_workers[count.index].disk_size
    }
  }


  metadata = {
    ssh-keys = var.list_vms_workers[count.index].ssh-keys
    #user-data = "${file("user-meta.txt")}"
  }

  network_interface {
    subnet_id = var.list_vms_workers[count.index].subnet_id
    nat       = true
  }

}
