output "instances_details_managers" {
  # count = length(var.list_vms)
  value = [
    for i in yandex_compute_instance.vms-managers :
    "${i.name} ansible_port=22 host_name=${i.hostname} ansible_internal_ip=${i.network_interface.0.ip_address} ansible_host=${i.network_interface.0.nat_ip_address} ansible_connection=ssh ansible_user=ubuntu swarm_port=4789"
  ]
}
output "instances_details_workers" {
  # count = length(var.list_vms)
  value = [
    for i in yandex_compute_instance.vms-workers :
    "${i.name} ansible_port=22 host_name=${i.hostname} ansible_internal_ip=${i.network_interface.0.ip_address} ansible_host=${i.network_interface.0.nat_ip_address} ansible_connection=ssh ansible_user=ubuntu swarm_port=4789"
  ]
}