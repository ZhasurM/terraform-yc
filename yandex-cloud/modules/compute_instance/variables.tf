variable "zone" {
  type    = string
  default = "ru-central1-a"
}

variable "auto_delete" {
  type        = bool
  default     = true
  description = "Defines whether the disk will be auto-deleted when the instance is deleted. The default value is True"
}

variable "list_vms_managers" {
  description = "Vm param"
  default = [
    {
      name          = "server-1"
      core_fraction = 5
      cores         = 1
      memory        = 2
      disk_size     = 20
      platform_id   = "standard-v2"
      image_id      = "fd8kb72eo1r5fs97a1ki"
      ssh-keys      = ""
      subnet_id     = ""
    }
  ]
}

variable "list_vms_workers" {
  description = "Vm param"
  default = [
    {
      name          = "server-1"
      core_fraction = 5
      cores         = 1
      memory        = 2
      disk_size     = 20
      platform_id   = "standard-v2"
      image_id      = "fd8kb72eo1r5fs97a1ki"
      ssh-keys      = ""
      subnet_id     = ""
    }
  ]
}